import 'package:flutter/foundation.dart';
import 'package:simple_permissions/simple_permissions.dart';

class StoragePermission with ChangeNotifier {
  PermissionStatus _readPermissionStatus;
  PermissionStatus _writePermissionStatus;
  bool _hasReadPermission;
  bool _hasWritePermission;

  StoragePermission() {
    _hasReadPermission = false;
    _hasWritePermission = false;
    _checkReadPermission();
    _checkWritePermission();
  }

  Future<void> requestReadPermission() async {
    _readPermissionStatus = await SimplePermissions.requestPermission(
        Permission.ReadExternalStorage);
    notifyListeners();
  }

  Future<void> requestWritePermission() async {
    _writePermissionStatus = await SimplePermissions.requestPermission(
        Permission.WriteExternalStorage);
    notifyListeners();
  }

  void _checkReadPermission() async {
    _hasReadPermission =
        await SimplePermissions.checkPermission(Permission.ReadExternalStorage);
    notifyListeners();
  }

  void _checkWritePermission() async {
    _hasWritePermission = await SimplePermissions.checkPermission(
        Permission.WriteExternalStorage);
    notifyListeners();
  }

  bool hasReadPermission() => _hasReadPermission;
  bool hasWritePermission() => _hasWritePermission;

  get readPermissionStatus => _readPermissionStatus;
  get writePermissionStatus => _writePermissionStatus;
}
