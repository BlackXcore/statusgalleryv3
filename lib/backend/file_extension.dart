class FileExtension {
  bool isFileJpg(String file) {
    bool _isImage = false;
    String _imageFormat = "jpg";

    String fileExtension = file.substring(file.length - 3, file.length);
    if (fileExtension == _imageFormat) _isImage = true;
    return _isImage;
  }

  bool isFileMp4(String file) {
    bool _isVideo = false;
    String _videoFormat = "mp4";

    String fileExtension = file.substring(file.length - 3, file.length);
    if (fileExtension == _videoFormat) _isVideo = true;
    return _isVideo;
  }

  bool isFileGif(String file) {
    bool _isGif = false;
    String _gifFormat = "gif";

    String fileExtension = file.substring(file.length - 3, file.length);
    if (fileExtension == _gifFormat) _isGif = true;
    return _isGif;
  }
}
