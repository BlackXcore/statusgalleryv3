import 'dart:io';
import 'package:flutter_share/flutter_share.dart';
import 'package:statusgalleryv3/backend/file_system.dart';

class ButtonsPanelController {
  Future<void> downloadFile(File file) async {
    if (!await AppFileSystem.doesFolderExist()) {
      await AppFileSystem.createFolder();
      // file.copy(AppFileSystem.getAppFolderPath()).catchError((onError) {
      //   print("Error: ButtonsPanelController: ${onError.toString()}");
      // });
    }

    //IF FOLDER EXISTS COPY FILE
    await AppFileSystem.createFolder();
    await file.copy(AppFileSystem.getAppFolderPath() + file.uri.pathSegments.last);
    print(AppFileSystem.getAppFolderPath());
  }

  Future<void> shareFile(File file) async {
    await FlutterShare.shareFile(
      title: file.uri.pathSegments.last,
      filePath: file.uri.path,
      text: "#StatusGallery"
    );
  }
}
