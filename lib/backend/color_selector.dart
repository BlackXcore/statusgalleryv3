import 'package:flutter/material.dart';

class ColorSelector with ChangeNotifier {
  BoxDecoration _mainDecoration;
  Color _mainColor;
  Color _mainTextColor;
  Color _mainIconColor;
  Color _secondaryTextColor;

  ColorSelector() {
    this._mainColor = Colors.grey[300];
    this._mainTextColor = Color.fromRGBO(171, 171, 171, 1);
    this._mainIconColor = Color.fromRGBO(171, 171, 171, 1);
    this._secondaryTextColor = Colors.grey.shade600;
    this.setLightMode();
  }

  BoxShadow _createShadow(Color color, Offset offset) {
    return BoxShadow(
      color: color,
      offset: offset,
      blurRadius: 15.0,
      spreadRadius: 1.0,
    );
  }

  void _setBoxDecoration({
    double borderRadius,
    BoxShadow topLeftShadow,
    BoxShadow bottomRightShadow,
  }) {
    this._mainDecoration = BoxDecoration(
      color: this._mainColor,
      borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
      boxShadow: [
        topLeftShadow,
        bottomRightShadow,
      ],
    );
  }

  set mainColor(Color color) => this._mainColor = color;

  void setDarkMode() {
    notifyListeners();
  }

  void setLightMode() {
    var _topLeftShadow = _createShadow(
      Colors.white,
      Offset(-4.0, -4.0),
    );

    var _bottomRightShadow = _createShadow(
      Colors.grey[500],
      Offset(4.0, 4.0),
    );

    _setBoxDecoration(
      borderRadius: 40.0,
      topLeftShadow: _topLeftShadow,
      bottomRightShadow: _bottomRightShadow,
    );

    notifyListeners();
  }

  get mainDecoration => this._mainDecoration;
  get mainColor => this._mainColor;
  get mainIconColor => this._mainIconColor;
  get mainTextColor => this._mainTextColor;
  get secondaryTextColor => this._secondaryTextColor;
}
