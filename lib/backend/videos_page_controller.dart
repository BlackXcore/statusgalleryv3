import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class VideosPageController with ChangeNotifier {
  bool _allThumbnailsCreated;
  List<String> _videoPaths;
  List<Uint8List> _thumbnailsInBytes;

  VideosPageController({@required List<String> videoPaths}) {
    this._allThumbnailsCreated = false;
    this._videoPaths = videoPaths;
    this._thumbnailsInBytes = new List<Uint8List>();
    this._createThumbnails();
  }

  Future<Uint8List> _createThumbnail(String videoPath) async {
    final _videoThumbnail = await VideoThumbnail.thumbnailData(
      video: videoPath,
      imageFormat: ImageFormat.JPEG,
      maxWidth: 128,
      maxHeight: 128,
      quality: 100,
    );

    return _videoThumbnail;
  }

  Future<void> _createThumbnails() async {
    print('Awe 1');

    for (var mp4Path in this._videoPaths) {
      final _videoInBytes = await _createThumbnail(mp4Path);
      print('Awe 2');
      _thumbnailsInBytes.add(_videoInBytes);
    }

    this._allThumbnailsCreated = true;
    print('Awe aWE 3');
    notifyListeners();
  }

  get allThumbnailsCreated => this._allThumbnailsCreated;
  List<Uint8List> videoThumbnails() => this._thumbnailsInBytes;
}
