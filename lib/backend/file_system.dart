import 'dart:io';

class AppFileSystem {
  static final String _appFolder = "/storage/emulated/0/Status Gallery/";

  static Future<bool> doesFolderExist() async {
    final _statusGallery = Directory(_appFolder);
    bool _folderExists = false;

    await _statusGallery.exists().then((onValue) {
      _folderExists = true;
    }).catchError(
      (onError) {
        _folderExists = false;
        print(onError);
      },
    );

    return _folderExists;
  }

  static Future<void> createFolder() async {
    await Directory(_appFolder).create(recursive: true).catchError((onError) {
      print(onError);
    });
  }

  static String getAppFolderPath() => _appFolder;
}
