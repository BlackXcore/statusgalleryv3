import 'package:flutter/foundation.dart';

//FILE FOMARTS
enum format { mp4, gif, jpg }

class FormatFocus with ChangeNotifier {
  format _currentFormatFocus;

  FormatFocus() {
    _currentFormatFocus = format.jpg;
  }

  //UPDATE CURRENT FORMAT FOCUSES
  set formatFocus(format fileExt) {
    _currentFormatFocus = fileExt;
    notifyListeners();
  }

  //GET CURRENT FOCUS
  get currentFormatFocus => _currentFormatFocus;
}
