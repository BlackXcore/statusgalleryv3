import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:statusgalleryv3/backend/file_extension.dart';
import 'package:flutter/material.dart';

class StorageAccess with ChangeNotifier {
  bool _filesFound;
  String _statusFilesPath;

//LIST PATHS
  List<String> _mp4Paths;
  List<String> _gifPaths;
  List<String> _jpgPaths;

  StorageAccess({bool initiateFiles = false}) {
    _filesFound = false;
    _mp4Paths = List<String>();
    _gifPaths = List<String>();
    _jpgPaths = List<String>();
    _statusFilesPath = "/storage/emulated/0/WhatsApp/Media/.Statuses";

    if (initiateFiles) initFiles();
  }

  void _getFilePaths() {
    FileExtension _fileExtension = FileExtension();
    List<FileSystemEntity> _dirContent = Directory(_statusFilesPath).listSync();

    for (FileSystemEntity file in _dirContent) {
      String _filePath = file.uri.toFilePath().toString();
      if (_fileExtension.isFileMp4(_filePath))
        _mp4Paths.add(_filePath);
      else if (_fileExtension.isFileGif(_filePath))
        _gifPaths.add(_filePath);
      else if (_fileExtension.isFileJpg(_filePath)) _jpgPaths.add(_filePath);
    }
  }

  void initFiles() {
    _getFilePaths();
    _filesFound =
        _mp4Paths.length > 0 || _gifPaths.length > 0 || _jpgPaths.length > 0;
    notifyListeners();
  }

//GETTERS FOR FILE PATHS
  get filesFound => _filesFound;
  List<String> mp4Paths() => _mp4Paths;
  get gifPaths => _gifPaths;
  List<String> jpgPaths() => _jpgPaths;

//CHECKERS
  get mp4FilesExist => _mp4Paths.length > 0;
  get gifFilesExist => _gifPaths.length > 0;
  get jpgFilesExist => _jpgPaths.length > 0;
}
