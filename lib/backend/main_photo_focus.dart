import 'dart:io';
import 'package:flutter/material.dart';

class MainPhotoFocus with ChangeNotifier {
  bool _isPhotoSelected;
  File _imageFile;

  MainPhotoFocus() {
    _isPhotoSelected = false;
  }

  void resetPhotoSelectedState(bool isPhotoSelected) =>
      this._isPhotoSelected = isPhotoSelected;

  void setSelectPhoto(File imageFile) {
    this._imageFile = imageFile;
    this._isPhotoSelected = true;
    notifyListeners();
  }

  bool get isPhotoSelected => _isPhotoSelected;
  File get selectedImageFile => _imageFile;
}
