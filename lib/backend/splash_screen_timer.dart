import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/view/home_page.dart';
import 'package:statusgalleryv3/backend/storage_access.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/backend/main_photo_focus.dart';
import 'package:statusgalleryv3/backend/storage_permission.dart';

class SplashScreenTimer with ChangeNotifier {
  Timer timer;
  BuildContext _context;

  SplashScreenTimer({
    @required int seconds,
    @required BuildContext context,
  }) {
    _context = context;
    _startCountDown(seconds);
  }

  void _startCountDown(int seconds) async {
    Timer(
      Duration(seconds: seconds),
      () async {
        final _storagePermission = Provider.of<StoragePermission>(_context);
        await _storagePermission.requestReadPermission();
        await _storagePermission.requestWritePermission();
        _navigateToHomePage();
      },
    );
  }

  void _navigateToHomePage() {
    Navigator.of(_context).pushReplacement(
      new CupertinoPageRoute(
        builder: (_context) => ChangeNotifierProvider(
          builder: (_) => ColorSelector(),
          child: Scaffold(
            body: ChangeNotifierProvider(
              builder: (_) => StorageAccess(initiateFiles: true),
              child: ChangeNotifierProvider(
                builder: (_) => MainPhotoFocus(),
                child: ChangeNotifierProvider(
                  builder: (_) => StoragePermission(),
                  child: HomePage(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
