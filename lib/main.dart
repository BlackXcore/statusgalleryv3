import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/backend/storage_access.dart';
import 'package:statusgalleryv3/backend/storage_permission.dart';
import 'package:statusgalleryv3/view/splash_screen.dart';

void main() {
  runApp(MyApp());
  SystemChrome.setEnabledSystemUIOverlays([]);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Status Gallery',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ChangeNotifierProvider(
        builder: (_) => StoragePermission(),
        child: ChangeNotifierProvider(
          child: StatusGalleryApp(),
          builder: (_) => StorageAccess(),
        ),
      ),
    );
  }
}

class StatusGalleryApp extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<StatusGalleryApp> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (_) => ColorSelector(),
      child: Scaffold(
        body: SplashScreen(),
      ),
    );
  }
}
