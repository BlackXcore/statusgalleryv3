import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/view/shadows.dart';

class SoftIconButton extends StatefulWidget {
  final IconData icon;
  final double size;
  final Function callBack;

  SoftIconButton({
    @required this.icon,
    @required this.size,
    @required this.callBack,
  });
  @override
  _SoftIconButtonState createState() => _SoftIconButtonState();
}

class _SoftIconButtonState extends State<SoftIconButton> {
  bool _update = false;
  @override
  Widget build(BuildContext context) {
    return _softIconButton();
  }

  @override
  void initState() {
    super.initState();
  }

  void _updateButtonState() {
    if (mounted) {
      setState(() {
        this._update = true;
      });
      Timer(Duration(milliseconds: 400), () {
        setState(() {
          this._update = false;
        });
      });
    }
  }

  Widget _softIconButton() {
    final colorSelector = Provider.of<ColorSelector>(context);

    return GestureDetector(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 10),
        curve: Curves.bounceIn,
        decoration: BoxDecoration(
          color:
              (_update) ? colorSelector.mainIconColor : colorSelector.mainColor,
          borderRadius: BorderRadius.circular(18),
          boxShadow: [
            Shadows.darkShadow(),
            Shadows.lightShadow(),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Icon(
            widget.icon,
            color: (_update)
                ? colorSelector.mainColor
                : colorSelector.mainIconColor,
            size: widget.size,
          ),
        ),
      ),
      onTap: () {
        _updateButtonState();
        widget.callBack();
      },
    );
  }
}
