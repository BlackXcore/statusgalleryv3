import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/view/shadows.dart';

class AlertUser {
  static showMessage(
    BuildContext context, {
    @required String title,
    @required String message,
  }) {
    final _colorSelector = Provider.of<ColorSelector>(context);

    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (_) => AlertDialog(
        title: _createText(
          context: context,
          text: title,
          size: 28,
        ),
        content: _createText(
          context: context,
          text: message,
          size: 22,
        ),
        backgroundColor: _colorSelector.mainColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        actions: <Widget>[
          _closeButton(context),
        ],
      ),
    );
  }

  static Widget _closeButton(BuildContext context) {
    final _colorSelector = Provider.of<ColorSelector>(context);

    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.only(
          right: 8.0,
          bottom: 8.0,
        ),
        child: Container(
          decoration: BoxDecoration(
            color: _colorSelector.mainColor,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              Shadows.darkShadow(),
              Shadows.lightShadow(),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Icon(
              Icons.cancel,
              color: _colorSelector.mainIconColor,
              size: 25,
            ),
          ),
        ),
      ),
      onTap: () {
        Navigator.pop(context);
      },
    );
  }

  static Widget _createText({
    @required BuildContext context,
    @required String text,
    @required double size,
  }) {
    final _colorSelector = Provider.of<ColorSelector>(context);

    return Text(
      text,
      style: TextStyle(
        color: _colorSelector.secondaryTextColor,
        fontSize: size,
      ),
    );
  }
}
