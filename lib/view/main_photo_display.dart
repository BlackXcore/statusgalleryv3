import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/view/shadows.dart';
import 'package:statusgalleryv3/view/view_selected_image.dart';

class MainPhotoDisplay extends StatefulWidget {
  final File selectedPhoto;

  MainPhotoDisplay(this.selectedPhoto);
  @override
  _MainPhotoDisplayState createState() => _MainPhotoDisplayState();
}

class _MainPhotoDisplayState extends State<MainPhotoDisplay> {
  @override
  Widget build(BuildContext context) {
    final _screenProperties = MediaQuery.of(context);
    final colorSelector = Provider.of<ColorSelector>(context);
    double _width = _screenProperties.size.width * 0.1;
    double _containerWidth = _screenProperties.size.width * 0.85;
    double _containerHeight = _screenProperties.size.height * 0.85;

    return Padding(
      padding: EdgeInsets.only(
        left: _width,
        right: _width,
        top: 17,
        bottom: 17,
      ),
      child: Container(
        width: _containerWidth,
        height: _containerHeight,
        decoration: BoxDecoration(
          color: colorSelector.mainColor,
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          boxShadow: [
            Shadows.darkShadow(),
            Shadows.lightShadow(),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: (widget.selectedPhoto != null)
                ? GestureDetector(
                    child: Image.file(
                      widget.selectedPhoto,
                      fit: BoxFit.cover,
                    ),
                    onTap: _navigateToViewSelectedImage,
                  )
                : Container(),
          ),
        ),
      ),
    );
  }

  void _navigateToViewSelectedImage() {
    Navigator.of(context).push(
      new CupertinoPageRoute(
        builder: (context) => ViewSelectedImage(widget.selectedPhoto),
      ),
    );
  }
}
