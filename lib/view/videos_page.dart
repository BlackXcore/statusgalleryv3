import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/view/nav_bar.dart';
import 'package:statusgalleryv3/view/play_selected_video.dart';
import 'package:statusgalleryv3/view/shadows.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/backend/videos_page_controller.dart';

class VideosPage extends StatefulWidget {
  final List<String> videoPaths;
  VideosPage({@required this.videoPaths});
  @override
  _VideosPageState createState() => _VideosPageState();
}

class _VideosPageState extends State<VideosPage> {
  @override
  Widget build(BuildContext context) {
    final _colorSelector = Provider.of<ColorSelector>(context);
    return Scaffold(
      backgroundColor: _colorSelector.mainColor,
      body: _videosPageWidgets(),
    );
  }

  Widget _videosPageWidgets() {
    return Container(
      child: Column(
        children: <Widget>[
          _navBar(),
          _videosPanel(),
        ],
      ),
    );
  }

  Widget _navBar() {
    return Expanded(
      flex: 4,
      child: NavBar(),
    );
  }

  Widget _videosPanel() {
    final _colorSelector = Provider.of<ColorSelector>(context);
    final _screenProperties = MediaQuery.of(context);
    double _width = _screenProperties.size.width;

    return Expanded(
      flex: 15,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 32.0,
          bottom: 0,
        ),
        child: Container(
          width: _width,
          decoration: BoxDecoration(
            color: _colorSelector.mainColor,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40.0),
              topRight: Radius.circular(40.0),
            ),
            boxShadow: [
              Shadows.darkShadow(),
              Shadows.lightShadow(),
            ],
          ),
          child: _videosGridView(),
        ),
      ),
    );
  }

  Widget _videosGridView() {
    final _videosPageController = Provider.of<VideosPageController>(context);

    return Padding(
      padding: const EdgeInsets.only(
        // top: 5,
        left: 35,
        right: 35,
      ),
      child: GridView.builder(
        itemCount: (_videosPageController.allThumbnailsCreated)
            ? _videosPageController.videoThumbnails().length
            : 0,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemBuilder: (context, index) {
          return (_videosPageController.allThumbnailsCreated)
              ? Container(
                  child: _panelVideo(
                    imageFile: _videosPageController.videoThumbnails()[index],
                    index: index,
                  ),
                )
              : Container();
        },
      ),
    );
  }

  Widget _panelVideo({
    @required Uint8List imageFile,
    @required int index,
  }) {
    final colorSelector = Provider.of<ColorSelector>(context);

    return Padding(
      padding: const EdgeInsets.only(
        top: 32.0,
        bottom: 18.0,
        left: 15.0,
        right: 15.0,
      ),
      child: Container(
        height: 180,
        width: 120,
        decoration: BoxDecoration(
          color: colorSelector.mainColor,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            Shadows.darkShadow(),
            Shadows.lightShadow(),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: GestureDetector(
            child: Image.memory(
              imageFile,
              fit: BoxFit.cover,
            ),
            onTap: () => _goToVideo(index),
          ),
        ),
      ),
    );
  }

  void _goToVideo(int index) {
    Navigator.of(context).push(
      new CupertinoPageRoute(
        builder: (context) => ChangeNotifierProvider(
          builder: (_) => ColorSelector(),
          child: PlaySelectedVideo(videoPath: widget.videoPaths[index]),
        ),
      ),
    );
  }
}
