import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/buttons_pannel_controller.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/view/alert_user.dart';
import 'package:statusgalleryv3/view/shadows.dart';
import 'package:statusgalleryv3/view/soft_icon_button.dart';
import 'package:video_player/video_player.dart';

class VideoButtonsPanel extends StatefulWidget {
  final File videoFile;
  final VideoPlayerController controller;

  VideoButtonsPanel({
    @required this.videoFile,
    @required this.controller,
  });
  @override
  _VideoButtonsPanelState createState() => _VideoButtonsPanelState();
}

class _VideoButtonsPanelState extends State<VideoButtonsPanel> {
  @override
  Widget build(BuildContext context) {
    final _screenProperties = MediaQuery.of(context);
    final _colorSelector = Provider.of<ColorSelector>(context);
    double _width = _screenProperties.size.width * 0.65;
    double _height = _screenProperties.size.height * 0.09;

    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        color: _colorSelector.mainColor,
        child: Container(
          height: _height,
          width: _width,
          child: _videoButtonsPanelWidgets(),
        ),
      ),
    );
  }

  Widget _videoButtonsPanelWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _restartButton(),
        SizedBox(width: 20),
        _downloadButton(),
        SizedBox(width: 20),
        _shareButton(),
        SizedBox(width: 20),
        _playPauseButton(),
      ],
    );
  }

  Widget _downloadButton() {
    ButtonsPanelController _buttonsPanelController = ButtonsPanelController();

    return _softIconButton(
      icon: Icons.get_app,
      callBack: () async {
        await _buttonsPanelController.downloadFile(widget.videoFile);
        AlertUser.showMessage(
          context,
          title: "Success",
          message: "File has been downloaded. \nCheck Status Gallery folder.",
        );
      },
    );
  }

  Widget _shareButton() {
    ButtonsPanelController _buttonsPanelController = ButtonsPanelController();

    return _softIconButton(
      icon: Icons.share,
      callBack: () async {
        await _buttonsPanelController.shareFile(widget.videoFile);
      },
    );
  }

  Widget _playPauseButton() {
    return _softIconButton(
      icon:
          (widget.controller.value.isPlaying) ? Icons.pause : Icons.play_arrow,
      callBack: _playPauseFunctionality,
    );
  }

  void _playPauseFunctionality() {
    setState(() {
      if (widget.controller.value.isPlaying) {
        widget.controller.pause();
      } else {
        widget.controller.play();
      }
    });
  }

  Widget _restartButton() {
    return _softIconButton(
      icon: Icons.replay,
      callBack: _restartButtonFunctionality,
    );
  }

  void _restartButtonFunctionality() {
    widget.controller.seekTo(
      Duration(seconds: 0),
    );
  }

  Widget _softIconButton({
    @required IconData icon,
    @required Function callBack,
  }) {
    return SoftIconButton(
      icon: icon,
      size: 25,
      callBack: callBack,
    );
  }
}
