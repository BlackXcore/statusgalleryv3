import 'dart:io';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ViewSelectedImage extends StatelessWidget {
  final File image;
  ViewSelectedImage(this.image);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PhotoView.customChild(
        child: Image.file(image),
      ),
    );
  }
}
