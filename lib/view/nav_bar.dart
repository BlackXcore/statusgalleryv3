import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/view/shadows.dart';

class NavBar extends StatefulWidget {
  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  @override
  Widget build(BuildContext context) {
    final _screenProperties = MediaQuery.of(context);
    final colorSelector = Provider.of<ColorSelector>(context);
    double _width = _screenProperties.size.width * 0.07;
    double _height = _screenProperties.size.height * 0.05;
    double _containerWidth = _screenProperties.size.width * 0.9;

    return Padding(
      padding: EdgeInsets.only(
        top: _height,
        bottom: _height,
        left: _width,
        right: _width,
      ),
      child: Container(
        width: _containerWidth,
        decoration: BoxDecoration(
          color: colorSelector.mainColor,
          borderRadius: BorderRadius.all(Radius.circular(20.0)),
          boxShadow: [
            Shadows.darkShadow(),
            Shadows.lightShadow(),
          ],
        ),
        child: _navBarItems(),
      ),
    );
  }

  Widget _navBarItems() {
    final _screenProperties = MediaQuery.of(context);
    double _width = _screenProperties.size.width * 0.75;

    return Container(
      width: _width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: _homeButtonIcon(),
              ),
              SizedBox(width: 15),
              _currentPageName(),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: _homepageSettingsMenu(),
          ),
        ],
      ),
    );
  }

  Widget _homeButtonIcon() {
    return _softIconButton(
      icon: Icons.home,
    );
  }

  Widget _softIconButton({IconData icon}) {
    final colorSelector = Provider.of<ColorSelector>(context);

    return Container(
      decoration: BoxDecoration(
        color: colorSelector.mainColor,
        borderRadius: BorderRadius.circular(18),
        boxShadow: [
          Shadows.darkShadow(),
          Shadows.lightShadow(),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Icon(
          icon,
          color: colorSelector.mainIconColor,
          size: 30,
        ),
      ),
    );
  }

  Widget _currentPageName() {
    final _colorSelector = Provider.of<ColorSelector>(context);

    return Text(
      "home",
      style: TextStyle(
        color: _colorSelector.mainTextColor,
        fontSize: 34,
      ),
    );
  }

  Widget _homepageSettingsMenu() {
    final _colorSelector = Provider.of<ColorSelector>(context);

    return Icon(
      Icons.settings,
      color: _colorSelector.mainIconColor,
      size: 35,
    );
  }
}
