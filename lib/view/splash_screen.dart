import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/backend/splash_screen_timer.dart';
import 'package:statusgalleryv3/view/shadows.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final colorSelector = Provider.of<ColorSelector>(context);

    return ChangeNotifierProvider(
      builder: (_) => SplashScreenTimer(
        seconds: 3,
        context: context,
      ),
      child: Scaffold(
        backgroundColor: colorSelector.mainColor,
        body: _statusGallerySplashScreen(),
      ),
    );
  }

  Widget _statusGallerySplashScreen() {
    final colorSelector = Provider.of<ColorSelector>(context);

    return Center(
      child: Container(
        child: _statusGalleryLogo(),
        width: 200,
        height: 200,
        decoration: BoxDecoration(
          color: colorSelector.mainColor,
          borderRadius: BorderRadius.all(Radius.circular(40.0)),
          boxShadow: [
            Shadows.darkShadow(),
            Shadows.lightShadow(),
          ],
        ),
      ),
    );
  }

  Widget _statusGalleryLogo() {
    // final colorSelector = Provider.of<ColorSelector>(context);
    String _fontFamily = "BungeeHairline";

    return Center(
      child: Text(
        "SG",
        style: TextStyle(
          fontFamily: _fontFamily,
          fontWeight: FontWeight.w500,
          fontSize: 120,
        ),
      ),
    );
  }
}
