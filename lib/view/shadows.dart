import 'package:flutter/material.dart';

class Shadows {
  static BoxShadow darkShadow() {
    return BoxShadow(
      color: Colors.grey[500],
      offset: Offset(4.0, 4.0),
      blurRadius: 8.0,
      spreadRadius: 1.0,
    );
  }

  static BoxShadow lightShadow() {
    return BoxShadow(
      color: Colors.white,
      offset: Offset(-4.0, -4.0),
      blurRadius: 8.0,
      spreadRadius: 1.0,
    );
  }
}
