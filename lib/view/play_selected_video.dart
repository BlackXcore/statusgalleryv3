import 'dart:io';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:statusgalleryv3/view/video_buttons_panel.dart';

class PlaySelectedVideo extends StatefulWidget {
  final String videoPath;

  PlaySelectedVideo({@required this.videoPath});
  @override
  _PlaySelectedVideoState createState() => _PlaySelectedVideoState();
}

class _PlaySelectedVideoState extends State<PlaySelectedVideo> {
  VideoPlayerController _videoPlayerController;
  File _videoFile;

  @override
  void initState() {
    _videoFile = File(widget.videoPath);

    _videoPlayerController = VideoPlayerController.file(_videoFile)
      ..setLooping(true);
    _videoPlayerController.initialize().then((onValue) {
      setState(() {
        _videoPlayerController.play();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: _video(),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: _buttonsPanel(),
          ),
        ],
      ),
    );
  }

  Widget _video() {
    return AspectRatio(
      aspectRatio: _videoPlayerController.value.aspectRatio,
      child: Container(
        color: Colors.black,
        child: GestureDetector(
          child: VideoPlayer(_videoPlayerController),
          onTap: _playOnTouch,
          onDoubleTap: _restartVideo,
        ),
      ),
    );
  }

  Widget _buttonsPanel() {
    return VideoButtonsPanel(
      videoFile: _videoFile,
      controller: _videoPlayerController,
    );
  }

  void _playOnTouch() {
    setState(() {
      if (_videoPlayerController.value.isPlaying) {
        _videoPlayerController.pause();
      } else {
        _videoPlayerController.play();
      }
    });
  }

  void _restartVideo() {
    _videoPlayerController.seekTo(
      Duration(seconds: 0),
    );
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    super.dispose();
  }
}
