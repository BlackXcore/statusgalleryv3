import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/backend/main_photo_focus.dart';
import 'package:statusgalleryv3/backend/storage_access.dart';
import 'package:statusgalleryv3/backend/storage_permission.dart';
import 'package:statusgalleryv3/view/buttons_panel.dart';
import 'package:statusgalleryv3/view/main_photo_display.dart';
import 'package:statusgalleryv3/view/nav_bar.dart';
import 'package:statusgalleryv3/view/shadows.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final colorSelector = Provider.of<ColorSelector>(context);

    return Container(
      child: Scaffold(
        backgroundColor: colorSelector.mainColor,
        body: _homepageWidgets(),
      ),
    );
  }

  Widget _homepageWidgets() {
    // Size _sizeProperties = MediaQuery.of(context).size;
    // double _maxScreenHeight = _sizeProperties.height;

    return Container(
      child: Column(
        children: <Widget>[
          _navBar(),
          _mainPhotoDisplay(),
          _buttonsPanel(),
          _photosPanel(),
        ],
      ),
    );
  }

  Widget _navBar() {
    return Expanded(
      flex: 4,
      child: NavBar(),
    );
  }

  Widget _mainPhotoDisplay() {
    final _mainPhotoFocus = Provider.of<MainPhotoFocus>(context);
    final _screenProperties = MediaQuery.of(context);
    final _colorSelector = Provider.of<ColorSelector>(context);
    double _width = _screenProperties.size.width * 0.8;

    return Expanded(
      flex: 9,
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: AnimatedCrossFade(
          firstChild: Container(
            width: _width,
            child: Center(
              child: Text(
                "Please select a photo to preview",
                style: TextStyle(
                  color: _colorSelector.mainTextColor,
                  fontSize: 40,
                  shadows: [
                    Shadows.darkShadow(),
                    Shadows.lightShadow(),
                  ],
                ),
              ),
            ),
          ),
          secondChild: MainPhotoDisplay(_mainPhotoFocus.selectedImageFile),
          crossFadeState: (_mainPhotoFocus.isPhotoSelected)
              ? CrossFadeState.showSecond
              : CrossFadeState.showFirst,
          duration: Duration(seconds: 1),
        ),
      ),
    );
  }

  Widget _buttonsPanel() {
    return Expanded(
      flex: 2,
      child: ButtonsPanel(),
    );
  }

  Widget _photosPanel() {
    final _storageAccess = Provider.of<StorageAccess>(context);
    final colorSelector = Provider.of<ColorSelector>(context);
    final _mainPhotoFocus = Provider.of<MainPhotoFocus>(context);
    final _storagePermission = Provider.of<StoragePermission>(context);

    return Expanded(
      flex: 4,
      child: Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: Container(
          decoration: BoxDecoration(
            color: colorSelector.mainColor,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(18.0),
              topRight: Radius.circular(18.0),
            ),
            boxShadow: [
              Shadows.darkShadow(),
              Shadows.lightShadow(),
            ],
          ),
          child: (_storagePermission.hasReadPermission())
              ? ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: _storageAccess.jpgPaths().length,
                  itemBuilder: (context, index) {
                    File _imageFile = File(_storageAccess.jpgPaths()[index]);

                    return GestureDetector(
                        child: _panelPhoto(imageFile: _imageFile),
                        onTap: () =>
                            _mainPhotoFocus.setSelectPhoto(_imageFile));
                  },
                )
              : Center(),
        ),
      ),
    );
  }

  Widget _panelPhoto({File imageFile}) {
    final colorSelector = Provider.of<ColorSelector>(context);

    return Padding(
      padding: const EdgeInsets.only(
        top: 32.0,
        bottom: 18.0,
        left: 15.0,
        right: 15.0,
      ),
      child: Container(
        height: 180,
        width: 120,
        decoration: BoxDecoration(
          color: colorSelector.mainColor,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            Shadows.darkShadow(),
            Shadows.lightShadow(),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Image.file(
            imageFile,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
