import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statusgalleryv3/backend/buttons_pannel_controller.dart';
import 'package:statusgalleryv3/backend/color_selector.dart';
import 'package:statusgalleryv3/backend/main_photo_focus.dart';
import 'package:statusgalleryv3/backend/storage_access.dart';
import 'package:statusgalleryv3/backend/videos_page_controller.dart';
import 'package:statusgalleryv3/view/alert_user.dart';
import 'package:statusgalleryv3/view/shadows.dart';
import 'package:statusgalleryv3/view/soft_icon_button.dart';
import 'package:statusgalleryv3/view/videos_page.dart';

class ButtonsPanel extends StatefulWidget {
  @override
  _ButtonsPanelState createState() => _ButtonsPanelState();
}

class _ButtonsPanelState extends State<ButtonsPanel> {
  @override
  Widget build(BuildContext context) {
    final _screenProperties = MediaQuery.of(context);
    // final colorSelector = Provider.of<ColorSelector>(context);
    double _width = _screenProperties.size.width * 0.75;

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        width: _width,
        child: Row(
          children: <Widget>[
            _downloadButton(),
            SizedBox(
              width: 20,
            ),
            _shareButton(),
            SizedBox(
              width: 20,
            ),
            _videosButtons(),
          ],
        ),
      ),
    );
  }

  Widget _downloadButton() {
    final _mainPhotoFocus = Provider.of<MainPhotoFocus>(context);
    ButtonsPanelController _buttonsPanelController = ButtonsPanelController();

    return _softIconButton(
        icon: Icons.get_app,
        callBack: () async {
          if (_mainPhotoFocus.isPhotoSelected) {
            await _buttonsPanelController
                .downloadFile(_mainPhotoFocus.selectedImageFile);
            AlertUser.showMessage(
              context,
              title: "Success",
              message:
                  "File has been downloaded. \nCheck Status Gallery folder.",
            );
          } else {
            AlertUser.showMessage(
              context,
              title: "Error",
              message: "Please select a photo first.",
            );
          }
        });
  }

  Widget _shareButton() {
    final _mainPhotoFocus = Provider.of<MainPhotoFocus>(context);
    ButtonsPanelController _buttonsPanelController = ButtonsPanelController();

    return _softIconButton(
      icon: Icons.share,
      callBack: () async {
        if (_mainPhotoFocus.isPhotoSelected) {
          await _buttonsPanelController
              .shareFile(_mainPhotoFocus.selectedImageFile);
        } else {
          AlertUser.showMessage(
            context,
            title: "Error",
            message: "Please select a photo first.",
          );
        }
      },
    );
  }

  Widget _videosButtons() {
    final _storageAccess = Provider.of<StorageAccess>(context);

    return GestureDetector(
      child: _softTextButton(),
      onTap: () {
        Navigator.of(context).push(
          new CupertinoPageRoute(
            builder: (context) => ChangeNotifierProvider(
              builder: (_) => ColorSelector(),
              child: ChangeNotifierProvider(
                builder: (_) => VideosPageController(
                  videoPaths: _storageAccess.mp4Paths(),
                ),
                child: VideosPage(
                  videoPaths: _storageAccess.mp4Paths(),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _softIconButton({
    @required IconData icon,
    @required Function callBack,
  }) {
    return SoftIconButton(
      icon: icon,
      size: 25,
      callBack: callBack,
    );
  }

  Widget _softTextButton({String text}) {
    final _colorSelector = Provider.of<ColorSelector>(context);

    return Container(
      decoration: BoxDecoration(
        color: _colorSelector.mainColor,
        borderRadius: BorderRadius.circular(18),
        boxShadow: [
          Shadows.darkShadow(),
          Shadows.lightShadow(),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: <Widget>[
            Text(
              "videos",
              style: TextStyle(
                color: _colorSelector.mainTextColor,
                fontSize: 24,
              ),
            ),
            SizedBox(width: 3),
            Icon(
              Icons.arrow_forward,
              color: _colorSelector.mainIconColor,
              size: 25,
            )
          ],
        ),
      ),
    );
  }
}
